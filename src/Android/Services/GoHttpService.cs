﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using Cirrious.CrossCore.Platform;

namespace Zorka.GoTrain.Android
{
	public class GoHttpService : IGoHttpService
	{
		CookieContainer cookies;

		public GoHttpService ()
		{

		}

		private async Task LoadCookies ()
		{
			if (cookies != null)
				return;
			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create (new Uri ("http://www.gotracker.ca/gotracker/web/"));
			request.ContentType = "application/json";
			request.Method = "GET";
			request.CookieContainer = new CookieContainer ();
			using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync ()) {
				cookies = request.CookieContainer;
			}
		}

		public  async Task<T> GetMainPage<T> (Func<Stream, Task<T>> processor)
		{
			await LoadCookies ();
			return await Get ("http://gotracker.ca/gotracker/web/", cookies, SetParameters, processor);
		}

		public  async Task<T> GetMapPage<T> (Func<Stream, Task<T>> processor)
		{
			await LoadCookies ();
			return await Get ("http://gotracker.ca/gotracker/web/GoMap.aspx", cookies, SetParameters, processor);
		}

		static void SetParameters (HttpWebRequest s)
		{
			s.Host = "www.gotracker.ca";
			s.UserAgent = @"Mozilla/5.0 (Windows NT 6.3; WOW64; rv"",3""4.0) Gecko/20100101 Firefox/34.0";
			s.Accept = @"application/xml, text/xml, * / *; q=0.01";
			s.Headers.Add ("Accept-Language", @"en-GB,en;q=0.5");
			s.Headers.Add ("Accept-Encoding", @"gzip, deflate");
			s.Headers.Add ("X-Requested-With", "XMLHttpRequest");
			s.Referer = @"http://www.gotracker.ca/gotracker/web/GoMap.aspx?mode=Public&followtrain=false&Language=en&stationId=0&Service=09&IncludeEquipmentTrip=false";
		}

		public async Task<T> GetTrainsInfo<T> (string corridor, Func<Stream, Task<T>> processor)
		{
			await LoadCookies ();
			return await Get ("http://gotracker.ca/GOTracker/web/GODataAPIProxy.svc/TripLocation/Service/Lang/"+corridor+"/en?_=1419126480477", cookies, SetParameters, processor);
		}

		private async Task<T> Get<T> (string url, CookieContainer cookies, Action<HttpWebRequest> settings, Func<Stream, Task<T>> loader)
		{
			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create (new Uri (url));
			request.ContentType = "application/json";
			request.Method = "GET";
			settings (request);

			request.CookieContainer = cookies;


			using (HttpWebResponse response = (HttpWebResponse)(await request.GetResponseAsync ())) {
				using (Stream stream = response.GetResponseStream ()) {
					if (response.StatusCode != HttpStatusCode.OK) {

						throw new Exception (response.StatusCode.ToString ());
					}

					return await loader (stream);
				}
			}
		}

		private string ReadAndDumpBody (string tag, Stream stream)
		{
			using (var tr = new StreamReader (stream)) {
				var body = tr.ReadToEnd ();
				MvxTrace.Trace ("{0} : {1}", tag, body);
				return body;
			}
		}
	}
}

