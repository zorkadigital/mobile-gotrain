using Cirrious.CrossCore.Plugins;

namespace Zorka.GoTrain.Android.Bootstrap
{
    public class DownloadCachePluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Plugins.DownloadCache.PluginLoader>
    {
    }
}