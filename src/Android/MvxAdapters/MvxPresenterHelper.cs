﻿using System;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore;
using System.Reflection;
using Xamarin.Forms;
using Cirrious.CrossCore.IoC;
using System.Linq;

namespace Zorka.GoTrain.Android
{	
	public static class MvxPresenterHelpers
	{
		public static IMvxViewModel LoadViewModel(MvxViewModelRequest request)
		{
			var viewModelLoader = Mvx.Resolve<IMvxViewModelLoader>();
			var viewModel = viewModelLoader.LoadViewModel(request, null);
			return viewModel;
		}
		public static ContentPage CreatePage(MvxViewModelRequest request)
		{
			var viewModelName = request.ViewModelType.Name;
			var pageName = viewModelName.Replace("ViewModel", "Page");
			var pageType = request.ViewModelType.GetTypeInfo().Assembly.CreatableTypes()
				.FirstOrDefault(t => t.Name == pageName);
			if (pageType == null)
			{
				Mvx.Trace("Page not found for {0}", pageName);
				return null;
			}
			var page = Activator.CreateInstance(pageType) as ContentPage;
			if (page == null)
			{
				Mvx.Error("Failed to create ContentPage {0}", pageName);
			}
			return page;
		}
	}
}

