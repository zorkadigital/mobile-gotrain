using System;
using Cirrious.MvvmCross.Droid.Views;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore;
using Android.App;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.OS;
using Cirrious.CrossCore.Droid.Views;
using System;
using Android.App;
using Android.Content;
using Android.OS;
using Cirrious.CrossCore.Core;
using Cirrious.CrossCore.Droid.Views;
using Xamarin.Forms.Platform.Android;
using Android.Content.PM;

namespace Zorka.GoTrain.Android
{

	[Activity(Label = "View for anyViewModel")]
	public class MvxNavigationActivity
		: AndroidActivity
	, IMvxPageNavigationProvider
	{
		private NavigationPage _navPage;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			Forms.Init(this, bundle);
			Xamarin.FormsMaps.Init (this, bundle);
			Mvx.Resolve<IMvxPageNavigationHost>().NavigationProvider = this;
			Mvx.Resolve<IMvxAppStart>().Start();
		}

		public async void Push(Page page)
		{
			if (_navPage != null)
			{
				await _navPage.PushAsync(page);
				return;
			}

			_navPage = new NavigationPage(page);
			SetPage(_navPage);
		}

		public async void Pop()
		{
			if (_navPage == null)
				return;

			await _navPage.PopAsync();
		}

		public NavigationPage NavigationPage { get { return _navPage; } }
	}

}
