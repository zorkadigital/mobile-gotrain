using System;
using Cirrious.MvvmCross.Droid.Views;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore;
using Android.App;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.OS;
using Cirrious.CrossCore.Droid.Views;
using System;
using Android.App;
using Android.Content;
using Android.OS;
using Cirrious.CrossCore.Core;
using Cirrious.CrossCore.Droid.Views;
using Xamarin.Forms.Platform.Android;
using Android.Content.PM;

namespace Zorka.GoTrain.Android
{
	
	public abstract class MvxFormsSplashScreenActivity
		: MvxSplashScreenActivity
	{
		protected MvxFormsSplashScreenActivity()
		{
		}

		protected MvxFormsSplashScreenActivity(int resourceId)
			: base(resourceId)
		{
		}

		public override void InitializationComplete()
		{
			StartActivity(typeof(MvxNavigationActivity));
		}
	}
}
