using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace Zorka.GoTrain
{
	public class GoCorridorInfo
	{
		public string Name { get; set; }
		public string Id { get; set; }

		public class Updated : MvxMessage {
			GoCorridorInfo[] corridors;

			public Updated (object sender, GoCorridorInfo[] corridors) : base(sender)
			{
				this.corridors = corridors;
			}

			public GoCorridorInfo[] Corridors {
				get {
					return this.corridors;
				}
			}
		}
	}
	
}