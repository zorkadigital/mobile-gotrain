﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace Zorka.GoTrain
{

	public class GoStopInfo
	{
		public string stop_name { get; set; }
		public double stop_lat { get; set; }
		public double stop_lon { get; set; }
		public string stop_url { get; set; }
		public string stop_short_name { get; set; }
		public int ATLS_LID { get; set; }
		public string ATLS_LOCATIONLABEL { get; set; }
		public object stop_code { get; set; }
		public string isYard { get; set; }

		public class Updated : MvxMessage {
			GoStopInfo[] stops;

			public Updated (object sender, GoStopInfo[] stops) : base(sender)
			{
				this.stops = stops;
			}
			public GoStopInfo[] Stops {
				get {
					return this.stops;
				}
			}
		}
	}
}