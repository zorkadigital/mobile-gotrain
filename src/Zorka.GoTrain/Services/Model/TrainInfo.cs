using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace Zorka.GoTrain
{

	public class GoTrainInfo {
		 
		public double Lat;
		public double Long;

		public class Updated : MvxMessage {
			GoTrainInfo[] trains;

			public Updated (object sender, GoTrainInfo[] trains) : base(sender)
			{
				this.trains = trains;
			}
			public GoTrainInfo[] Trains {
				get {
					return this.trains;
				}
			}
		}
	}
}
