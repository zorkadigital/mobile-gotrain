﻿using System;
using System.Net;
using System.Threading.Tasks;
using GTFS.IO;
using GTFS;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.File;
using System.Linq;
using Cheesebaron.MvxPlugins.ModernHttpClient;
using Cirrious.MvvmCross.Plugins.DownloadCache;

namespace Zorka.GoTrain
{
	public class ScheduleService
	{
		IZipService zipService;

		IMvxHttpFileDownloader fileDownloader;

		IMvxFileStore fileStore;

		public ScheduleService (IZipService zipService, IMvxHttpFileDownloader fileDownloader, IMvxFileStore fileStore)
		{
			this.fileStore = fileStore;
			this.fileDownloader = fileDownloader;
			this.zipService = zipService;
			Init ();
		}

		void  OnSuccess()
		{
			zipService.Unzip ("tmp/gtfs/google_transit.zip", "tmp/gtfs/gt");

			Mvx.Trace ("Found agency: {0}", _feed.GetAgencies ().Count());
		}

		void OnError (Exception obj)
		{
			Mvx.Error ("Error processing transit stream: {0}", obj);

		}

		private void Init()
		{
			fileStore.EnsureFolderExists ("tmp/gtfs");
			fileDownloader.RequestDownload("http://gotransit.com/publicroot/gtfs/google_transit.zip", "tmp/gtfs/google_transit.zip", OnSuccess, OnError);
		}
	}

}

