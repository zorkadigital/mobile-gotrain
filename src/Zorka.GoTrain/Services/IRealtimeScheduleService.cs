using System;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.DownloadCache;
using Cirrious.MvvmCross.Plugins.Network.Rest;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.IO;
using Cirrious.CrossCore.Platform;

namespace Zorka.GoTrain
{

	public interface IRealtimeScheduleService
	{
		Task Init ();

		Task RequestTrainsUpdate (string id);
	}
	
}
