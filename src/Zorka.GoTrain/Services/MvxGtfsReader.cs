using System;
using System.Net;
using System.Threading.Tasks;
using GTFS.IO;
using GTFS;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.File;
using System.Linq;
using Cheesebaron.MvxPlugins.ModernHttpClient;
using Cirrious.MvvmCross.Plugins.DownloadCache;

namespace Zorka.GoTrain
{
	public class MvxGtfsReader : IEnumerable<IGTFSSourceFile> {

		private List<GTFSSourceFileLines> _files;

		IMvxFileStore fileStore;

		IEnumerable<string> GetLinesReader (string f)
		{
			using(var stream = fileStore.OpenRead (f))
			using(var tr = new StreamReader(stream))
			{
				while (!tr.EndOfStream) {
					yield return tr.ReadLine ();
				}
			}
		}

		public MvxGtfsReader (IMvxFileStore fileStore, string folderName)
		{
			this.fileStore = fileStore;
			var files = fileStore.GetFilesIn (folderName);

			_files = files.Select (f => new GTFSSourceFileLines (GetLinesReader (f), Path.GetFileNameWithoutExtension(f))).ToList();
		}
		public IEnumerator<IGTFSSourceFile> GetEnumerator ()
		{
			return _files.GetEnumerator ();
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return _files.GetEnumerator ();
		}
	}

}
