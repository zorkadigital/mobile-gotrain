using System;
using System.Net;
using System.Threading.Tasks;
using GTFS.IO;
using GTFS;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.File;
using System.Linq;
using Cheesebaron.MvxPlugins.ModernHttpClient;
using Cirrious.MvvmCross.Plugins.DownloadCache;

namespace Zorka.GoTrain
{
	public interface IGoHttpService {
		Task<T> GetMapPage<T> (Func<Stream, Task<T>> processor);
		Task<T> GetTrainsInfo<T> (string corridor, Func<Stream, Task<T>> processor);
		Task<T> GetMainPage<T> (Func<Stream, Task<T>> processor);
	}
}
