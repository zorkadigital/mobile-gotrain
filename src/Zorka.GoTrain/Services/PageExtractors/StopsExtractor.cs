using System;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.DownloadCache;
using Cirrious.MvvmCross.Plugins.Network.Rest;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.IO;
using Cirrious.CrossCore.Platform;

namespace Zorka.GoTrain
{

	public class StopsExtractor
	{
		private static readonly Regex StopsStartRegex = new Regex (@"var\s+strStations\s+=\s+'");
		List<GoStopInfo> stops;

		public StopsExtractor ()
		{
			stops = new List<GoStopInfo> ();
		}

		public void ProcessLine (string line)
		{
			var match = StopsStartRegex.Match (line);
			if (!match.Success)
				return;
			var start = match.Index + match.Length;
			var end = line.LastIndexOf ("'");
			if (start >= end)
				return;
			var jsonBody = line.Substring (start, end - start).Replace (@"\'", "'");
			var stops = JsonConvert.DeserializeObject<GoStopInfo[]> (jsonBody);
			this.stops.AddRange (stops);
		}

		public List<GoStopInfo> Get ()
		{
			return stops;
		}
	}
	
}
