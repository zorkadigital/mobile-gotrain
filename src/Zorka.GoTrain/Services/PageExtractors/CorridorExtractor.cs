using System;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.DownloadCache;
using Cirrious.MvvmCross.Plugins.Network.Rest;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.IO;
using Cirrious.CrossCore.Platform;

namespace Zorka.GoTrain
{

	public class CorridorExtractor
	{
		List<GoCorridorInfo> corridors = new List<GoCorridorInfo> ();
		private static readonly Regex ValueRegex = new Regex ("<option.*value=\"(?<id>\\d+)\">(?<name>.+)</option>", RegexOptions.CultureInvariant);

		enum State
		{
			WaitingForOpenTag,
			Reading,
			Done

		}

		State state = State.WaitingForOpenTag;

		public CorridorExtractor ()
		{

		}

		public void ProcessLine (string line)
		{
			switch (state) {
			case State.WaitingForOpenTag:
				if (line.Contains ("$ddCorridor"))
					state = State.Reading;
				break;
			case State.Reading:
				if (line.Contains ("</select>")) {
					state = State.Done;
					break;
				} else {
					var match = ValueRegex.Match (line);
					if (!match.Success)
						break;
					var id = match.Groups ["id"].Value;
					var name = match.Groups ["name"].Value;
					corridors.Add (new GoCorridorInfo (){ Id = id, Name = name });
				}
				break;
			}

		}

		public List<GoCorridorInfo> Get ()
		{
			return corridors;
		}
	}
	
}
