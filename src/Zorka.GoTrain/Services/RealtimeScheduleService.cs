﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.DownloadCache;
using Cirrious.MvvmCross.Plugins.Network.Rest;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.IO;
using Cirrious.CrossCore.Platform;

namespace Zorka.GoTrain
{
	public class RealtimeScheduleService : IRealtimeScheduleService
	{
		IGoHttpService httpClient;
		IMvxMessenger messenger;

		List<GoStopInfo> stops = new List<GoStopInfo> ();
		List<GoCorridorInfo> corridors = new List<GoCorridorInfo> ();

		public RealtimeScheduleService ()
		{
			httpClient = Mvx.Resolve<IGoHttpService> ();
			messenger = Mvx.Resolve<IMvxMessenger> ();
		}

		private async Task LoadTrainsInfo (string corridor)
		{
			var trains = await httpClient.GetTrainsInfo (corridor, async (s) => {

				var doc = XDocument.Load (s);
				var trainDocs = doc.Element ("ReturnValueOfListOfInServiceTripPublic").Element ("Data").Elements ("InServiceTripPublic");

				var result =  trainDocs.Select (e => new GoTrainInfo () {
					Lat = double.Parse (e.Attribute ("Latitude").Value), 
					Long = double.Parse (e.Attribute ("Longitude").Value), 
				}).ToArray ();
				return result;
			});
			messenger.Publish(new GoTrainInfo.Updated(this, trains));
		}

		private async Task LoadStopsAndCorridors ()
		{
			corridors = await httpClient.GetMainPage (async (s) => {
				var corridorExtractor = new CorridorExtractor ();
				using (var sr = new StreamReader (s)) {
					for (var line = ""; line != null; line = await sr.ReadLineAsync ()) {
						corridorExtractor.ProcessLine (line);
					}
				}
				return corridorExtractor.Get ();
			});
			messenger.Publish (new GoCorridorInfo.Updated (this, corridors.ToArray()));

			stops = await httpClient.GetMapPage (async (s) => {
				var stopsExtractor = new StopsExtractor ();
				using (var sr = new StreamReader (s)) {
					for (var line = ""; line != null; line = await sr.ReadLineAsync ()) {
						stopsExtractor.ProcessLine (line);
					}
				}
				return stopsExtractor.Get ();
			});
			messenger.Publish (new GoStopInfo.Updated (this, stops.ToArray()));
		}

		public async Task Init ()
		{
			try {
				await LoadStopsAndCorridors ();
				if (corridors.Any ()) {
					await LoadTrainsInfo (corridors.First ().Id);
				}
			} catch (Exception ex) {
				Mvx.Trace ("Error {0}", ex.Message);	
			}
		}

		public Task RequestTrainsUpdate (string id)
		{
			return LoadTrainsInfo (id);
		}
	}
}

