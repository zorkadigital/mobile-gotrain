﻿using System;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Linq;
using Xamarin.Forms.Maps;
using System.Collections.Generic;

namespace Zorka.GoTrain.ViewModels
{
	public class MainViewModel : MvxViewModel
	{
		private readonly List<MvxSubscriptionToken> subscriptions = new List<MvxSubscriptionToken> ();

		private readonly IRealtimeScheduleService realtimeScheduleService;

		GoTrainInfo[] trains;
		MapSpan span;
		GoCorridorInfo[] corridors;
		GoCorridorInfo selectedCorridor;

		GoStopInfo[] stops;

		public GoTrainInfo[] Trains { get{ return trains; } set{ trains = value; base.RaisePropertyChanged (() => Trains); }}
		public MapSpan Span { get{ return span; } set{ span = value; base.RaisePropertyChanged (() => Span); }}
		public GoCorridorInfo[] Corridors { get { return corridors; } set { corridors = value; base.RaisePropertyChanged (() => Corridors); } }
		public GoCorridorInfo SelectedCorridor { get { return selectedCorridor; } set { selectedCorridor = value; base.RaisePropertyChanged (() => SelectedCorridor); } }

		public MainViewModel ()
		{
			realtimeScheduleService = Mvx.Resolve<IRealtimeScheduleService> ();
			var messenger = Mvx.Resolve<IMvxMessenger> ();
			subscriptions.Add (messenger.Subscribe<GoCorridorInfo.Updated> (HandleCorridorMessage));
			subscriptions.Add (messenger.Subscribe<GoStopInfo.Updated> (HandleStopsMessage));
			subscriptions.Add (messenger.Subscribe<GoTrainInfo.Updated>(OnTrainsUpdated));
			realtimeScheduleService.Init ();
		}

		void OnTrainsUpdated (GoTrainInfo.Updated obj)
		{
			Trains = obj.Trains;
		}

		void SetMapRegion (GoStopInfo[] stops)
		{
			var lats = stops.Select (q => (q.stop_lat));
			var lons = stops.Select (q => (q.stop_lon));
			Span = new MapSpan (new Position ((lats.Max () + lats.Min ()) / 2, (lons.Max () + lons.Min ()) / 2), lats.Max () - lats.Min (), lons.Max () - lons.Min ());
		}

		void OnCorridorChanged ()
		{

		}

		void HandleCorridorMessage (GoCorridorInfo.Updated obj)
		{
			Corridors = obj.Corridors;
			if(Corridors.Any())
				SelectedCorridor = Corridors.First ();
		}

		void HandleStopsMessage (GoStopInfo.Updated obj)
		{
			stops = obj.Stops;
			SetMapRegion (stops);
		}

		public void UiSelectedCorridorChanged (GoCorridorInfo goCorridorInfo)
		{
			selectedCorridor = goCorridorInfo;
			if(selectedCorridor != null)
				realtimeScheduleService.RequestTrainsUpdate (SelectedCorridor.Id);
		}
	}
}

