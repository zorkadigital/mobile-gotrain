﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Zorka.GoTrain.ViewModels;
using System.Linq;
using Cirrious.CrossCore.Platform;

namespace Zorka.GoTrain
{
	public partial class MainPage : ContentPage
	{
		private Map _map { get { return map; } }

		private MainViewModel ViewModel { get { return (MainViewModel)BindingContext; } }

		public MainPage ()
		{
			InitializeComponent ();

			_map.HasScrollEnabled = true;
			_map.HasZoomEnabled = true;

			corridor.SelectedIndexChanged += (object sender, EventArgs e) => ViewModel.UiSelectedCorridorChanged (ViewModel.Corridors [corridor.SelectedIndex]);
		}

		protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();

			ViewModel.PropertyChanged += HandlePropertyChanged;
			;
		}

		void HandlePropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "Trains") {
				_map.Pins.Clear ();
				foreach (var train in ViewModel.Trains) {
					_map.Pins.Add (new Pin () { 
						Label = "Train",
						Type = PinType.Generic, 
						Position = new Position (train.Lat, train.Long)
					});
				}

			} else if (e.PropertyName == "Span") {
				_map.MoveToRegion (ViewModel.Span);
			} else if (e.PropertyName == "Corridors") {
				corridor.Items.Clear ();
				foreach (var c in ViewModel.Corridors) {
					corridor.Items.Add (c.Name);
				}
			} else if (e.PropertyName == "SelectedCorridor") {
				corridor.SelectedIndex = Array.IndexOf (ViewModel.Corridors, ViewModel.SelectedCorridor);
			}
		}
	}
}

